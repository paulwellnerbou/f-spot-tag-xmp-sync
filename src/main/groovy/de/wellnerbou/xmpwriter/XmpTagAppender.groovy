package de.wellnerbou.xmpwriter

class XmpTagAppender {

    Node read(InputStream inputStream) {
        def parser = new XmlParser()
        def xml = parser.parse(inputStream)
    }

    void write(OutputStream outputStream, Node xml) {
        PrintWriter printWriter = new PrintWriter(outputStream)
        new XmlNodePrinter(printWriter).print(xml)
        printWriter.flush()
    }

    def append(Node xml, List<String> tags) {
        Node seqNodeDcSubject = xml.'rdf:RDF'.'rdf:Description'.'dc:subject'.'rdf:Seq'[0]
        addLi(tags, seqNodeDcSubject)
        Node seqNodeLr = xml.'rdf:RDF'.'rdf:Description'.'lr:hierarchicalSubject'.'rdf:Seq'[0]
        if(seqNodeLr != null) {
            addLi(tags, seqNodeLr)
        }
    }

    private List<String> addLi(List<String> tags, seqNode) {
        tags.each {
            seqNode.appendNode("rdf:li", it)
        }
    }
}
