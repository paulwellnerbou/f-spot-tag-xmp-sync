package de.wellnerbou.xmpwriter

import groovy.text.XmlTemplateEngine

class NewXmpFileWriter {

    final engine = new XmlTemplateEngine()
    final tpl = engine.createTemplate(getClass().classLoader.getResource("xmp-template.xmp") as URL)

    String createXmp(File img, String[] tags) {
        def sb = new StringBuilder()
        sb << tpl.make(['filename': img.name, 'tags': tags]  )
    }
}
