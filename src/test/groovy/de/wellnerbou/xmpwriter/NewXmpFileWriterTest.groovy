package de.wellnerbou.xmpwriter

import spock.lang.Specification

class NewXmpFileWriterTest extends Specification {
    def "test write"() {
        given:
        NewXmpFileWriter xmpFileWriter = new NewXmpFileWriter()

        when:
        def xmp = xmpFileWriter.createXmp(new File('/tmp/imagefile.jpg'), 'tag one', 'tag two')

        then:
        xmp != null
        xmp == '''<x:xmpmeta xmlns:x='adobe:ns:meta/' x:xmptk='XMP Core 4.4.0-Exiv2'>
  <rdf:RDF xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'>
    <rdf:Description rdf:about='' xmp:Rating='1' xmlns:xmp='http://ns.adobe.com/xap/1.0/' xmpMM:DerivedFrom='imagefile.jpg' xmlns:xmpMM='http://ns.adobe.com/xap/1.0/mm/'>
      <dc:subject xmlns:dc='http://purl.org/dc/elements/1.1/'>
        <rdf:Seq>
          <rdf:li>
            tag one
          </rdf:li>
        </rdf:Seq>
        <rdf:Seq>
          <rdf:li>
            tag two
          </rdf:li>
        </rdf:Seq>
      </dc:subject>
      <lr:hierarchicalSubject xmlns:lr='http://ns.adobe.com/lightroom/1.0/'>
        <rdf:Seq>
          <rdf:li>
            tag one
          </rdf:li>
        </rdf:Seq>
        <rdf:Seq>
          <rdf:li>
            tag two
          </rdf:li>
        </rdf:Seq>
      </lr:hierarchicalSubject>
    </rdf:Description>
  </rdf:RDF>
</x:xmpmeta>
'''
    }
}
