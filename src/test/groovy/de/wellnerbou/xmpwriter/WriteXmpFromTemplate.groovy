package de.wellnerbou.xmpwriter

import groovy.text.XmlTemplateEngine

final engine = new XmlTemplateEngine()
final tpl = engine.createTemplate(new File("src/test/resources/xmp-template.xmp"))

def sb = new StringBuilder()
sb << tpl.make(['filename': 'test.jpg','tags': ['one', 'two']]  )
println sb