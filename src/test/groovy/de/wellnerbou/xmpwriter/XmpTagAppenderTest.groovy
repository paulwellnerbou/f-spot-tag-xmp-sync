package de.wellnerbou.xmpwriter

import groovy.xml.QName
import spock.lang.Specification

class XmpTagAppenderTest extends Specification {
    def "test read"() {
        given:
        XmpTagAppender xmpTagAppender = new XmpTagAppender()

        when:
        Node xml = xmpTagAppender.read(getClass().classLoader.getResourceAsStream("standard.xmp"))

        then:
        xml.'rdf:RDF'.'rdf:Description'.'dc:subject'.'rdf:Seq'.'rdf:li'[0].text() == 'tag one'
        xml.'rdf:RDF'.'rdf:Description'.'dc:subject'.'rdf:Seq'.'rdf:li'[1].value()[0] == 'tag two'
    }

    def "test write"() {
        given:
        XmpTagAppender xmpTagAppender = new XmpTagAppender()

        when:
        Node xml = xmpTagAppender.read(getClass().classLoader.getResourceAsStream("standard-wo-lr.xmp"))
        def out = new ByteArrayOutputStream()
        xmpTagAppender.write(out, xml)

        then:
        out.toString() == '''<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="XMP Core 4.4.0-Exiv2">
  <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
    <rdf:Description rdf:about="" xmp:Rating="1" xmlns:xmp="http://ns.adobe.com/xap/1.0/" xmpMM:DerivedFrom="imagefile.jpg" xmlns:xmpMM="http://ns.adobe.com/xap/1.0/mm/">
      <dc:subject xmlns:dc="http://purl.org/dc/elements/1.1/">
        <rdf:Seq>
          <rdf:li>
            tag one
          </rdf:li>
          <rdf:li>
            tag two
          </rdf:li>
        </rdf:Seq>
      </dc:subject>
    </rdf:Description>
  </rdf:RDF>
</x:xmpmeta>
'''
    }

    def "test append"() {
        given:
        XmpTagAppender xmpTagAppender = new XmpTagAppender()

        when:
        Node xml = xmpTagAppender.read(getClass().classLoader.getResourceAsStream("standard-wo-lr.xmp"))
        xmpTagAppender.append(xml, ['tag three', 'tag four'])
        def out = new ByteArrayOutputStream()
        xmpTagAppender.write(out, xml)

        then:
        out.toString() == '''<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="XMP Core 4.4.0-Exiv2">
  <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
    <rdf:Description rdf:about="" xmp:Rating="1" xmlns:xmp="http://ns.adobe.com/xap/1.0/" xmpMM:DerivedFrom="imagefile.jpg" xmlns:xmpMM="http://ns.adobe.com/xap/1.0/mm/">
      <dc:subject xmlns:dc="http://purl.org/dc/elements/1.1/">
        <rdf:Seq>
          <rdf:li>
            tag one
          </rdf:li>
          <rdf:li>
            tag two
          </rdf:li>
          <rdf:li>
            tag three
          </rdf:li>
          <rdf:li>
            tag four
          </rdf:li>
        </rdf:Seq>
      </dc:subject>
    </rdf:Description>
  </rdf:RDF>
</x:xmpmeta>
'''
    }
}
